var express = require('express');
var router = express.Router();

/* GET test listing. */
router.get(['/', '/:id'], function(req, res, next) {
  if(req.params.id){
  	res.send('This is a get Message with id: '+ req.params.id);	
  }
  else{
  	res.send('This is a get Message without id');
  }
});

/* POST test listing. */
router.post('/', function(req, res, next) {
  res.send('This is a post Message with params: '+req.body);
});

/* DELETE test listing. */
router.delete('/:id', function(req, res, next) {
  res.send('This is a delete Message with id: '+req.params.id);
});

/* PUT test listing. */
router.put('/:id', function(req, res, next) {
	res.write('This is a put Message with id: '+req.params.id);
	res.write('And params: '+JSON.stringify(req.body));
  res.end();
});

module.exports = router;
